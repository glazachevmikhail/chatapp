import 'package:chatapp/auth/auth_page/widgets/page_info_widget.dart';
import 'package:flutter/material.dart';

import '../../main.dart';
import '../auth_page/widgets/circle_indicator.dart';
import '../auth_screen/widgets/button_auth_widget.dart';
import '../auth_screen_mobile/auth_screen_mobile_widget.dart';

class AuthPageMobileWidget extends StatelessWidget {
  const AuthPageMobileWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(37, 179, 114, 1),
      body: Column(
        children: [
          const SizedBox(height: 47),
          const CircleIndicator(
            currentIndex: 0,
            countIndicators: 3,
            currentIndicatorColor: Color.fromRGBO(37, 43, 54, 1),
            colorIndicator: Colors.white,
          ),
          PageInfoWidget(
              count: 3,
              widget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  SizedBox(
                    width: 128.94,
                    height: 129,
                    child: Placeholder(),
                  ),
                  SizedBox(height: 43),
                  Text(
                    'Добро пожаловать',
                    style: TextStyle(fontSize: 27, color: Colors.white),
                  ),
                  Text(
                    'в Dialog!',
                    style: TextStyle(
                        fontSize: 27,
                        color: Colors.white,
                        fontWeight: FontWeight.w300),
                  ),
                  SizedBox(height: 40),
                ],
              )),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 33),
            child: ButtonAuthWidget(
              height: 57,
              radius: 17,
              buttonColor: Colors.white,
              textColor: Colors.black,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        const AuthScreenMobileWidget(), // Переход на второй экран
                  ),
                );
              },
              buttonText: 'Далее',
            ),
          ),
          SizedBox(height: 40),
        ],
      ),
    );
  }
}
