import 'package:flutter/material.dart';

class PageInfoWidget extends StatelessWidget {
  final int count;
  final Widget widget;
  const PageInfoWidget({Key? key, required this.widget, required this.count})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController();

    return Expanded(
      child: PageView(
        controller: controller,
        children: List.generate(
          count,
          (index) => Padding(
              padding: const EdgeInsets.only(right: 50, left: 50, bottom: 45),
              child: widget),
        ),
      ),
    );
  }
}
