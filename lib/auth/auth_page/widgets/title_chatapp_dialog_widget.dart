import 'package:flutter/material.dart';

class TitleChatAppDialogWidget extends StatelessWidget {
  const TitleChatAppDialogWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(width: 51, height: 23, color: Colors.blue),
        const SizedBox(width: 10),
        const Text(
          'ChatApp Dialog',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
        ),
      ],
    );
  }
}
