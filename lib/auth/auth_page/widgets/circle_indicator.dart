import 'package:flutter/material.dart';

class CircleIndicator extends StatelessWidget {
  final int currentIndex;
  final int countIndicators;
  final Color currentIndicatorColor;
  final Color colorIndicator;
  final double currentIndicatorSize;
  final double indicatorSize;

  const CircleIndicator({
    super.key,
    required this.currentIndex,
    required this.countIndicators,
    this.currentIndicatorColor = Colors.blue,
    this.colorIndicator = Colors.grey,
    this.currentIndicatorSize = 13.0,
    this.indicatorSize = 13.0,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(
        countIndicators,
        (index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 13.0),
          child: Container(
            width: index == currentIndex ? currentIndicatorSize : 13.0,
            height: index == currentIndex ? currentIndicatorSize : 13.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: index == currentIndex
                  ? currentIndicatorColor // цвет активного индикатора
                  : colorIndicator, // цвет неактивных индикаторов
            ),
          ),
        ),
      ),
    );
  }
}
