import 'package:chatapp/auth/auth_page/widgets/circle_indicator.dart';
import 'package:chatapp/auth/auth_page/widgets/info_text_widget.dart';
import 'package:chatapp/auth/auth_page/widgets/page_info_widget.dart';
import 'package:chatapp/auth/auth_page/widgets/title_chatapp_dialog_widget.dart';
import 'package:flutter/material.dart';

class InfoPageScreenWidget extends StatelessWidget {
  const InfoPageScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        color: const Color.fromRGBO(247, 248, 248, 1),
        child: Column(
          children: [
            const SizedBox(height: 46),
            const TitleChatAppDialogWidget(),
            const SizedBox(height: 52),
            PageInfoWidget(
              count: 5,
              widget: Container(
                color: Colors.blue,
              ),
            ),
            const InfoTextWidget(),
            const SizedBox(height: 60),
            const CircleIndicator(
                currentIndex: 2,
                countIndicators: 5,
                currentIndicatorSize: 23.0,
                currentIndicatorColor: Color.fromRGBO(37, 179, 114, 1),
                colorIndicator: Color.fromRGBO(216, 224, 237, 1)),
            const SizedBox(height: 60),
          ],
        ),
      ),
    );
  }
}
