import 'package:flutter/material.dart';

import '../auth_screen/widgets/FotgotThePassword.dart';
import '../auth_screen/widgets/button_auth_widget.dart';
import '../auth_screen/widgets/or_divider_widget.dart';
import '../auth_screen/widgets/text_title_widget.dart';

class AuthScreenMobileWidget extends StatelessWidget {
  const AuthScreenMobileWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(
            height: 414,
            child: Placeholder(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 25),
                const TextTitleWidget(text: 'Вход', size: 27.0),
                const SizedBox(height: 25),
                const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 25),
                const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: const [
                    ForgotThePassword(),
                  ],
                ),
                const SizedBox(height: 28),
                ButtonAuthWidget(
                  height: 57,
                  radius: 17,
                  buttonColor: const Color.fromRGBO(37, 43, 54, 1),
                  textColor: Colors.white,
                  onPressed: () {},
                  buttonText: 'Войти',
                ),
                const SizedBox(height: 32),
                const OrDividerWidget(
                  height: 3,
                  width: 60,
                  colorDivider: Color.fromRGBO(0, 0, 0, 0.11),
                  color: Color.fromRGBO(0, 0, 0, 0.6),
                  text: 'Или',
                  textSize: 14,
                ),
                const SizedBox(height: 25),
                const SizedBox(
                  height: 40,
                  child: Placeholder(),
                ),
                const SizedBox(height: 33),
                ButtonAuthWidget(
                  onPressed: () {},
                  buttonText: 'Зарегестрироваться',
                  buttonColor: const Color.fromRGBO(37, 179, 114, 1),
                  textColor: Colors.white,
                  height: 57,
                  radius: 17,
                ),
                const SizedBox(height: 33),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
