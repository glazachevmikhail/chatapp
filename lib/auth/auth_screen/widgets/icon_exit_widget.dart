import 'package:flutter/material.dart';

class IconExitWidget extends StatelessWidget {
  const IconExitWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 40),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          IconButton(
            onPressed: () {},
            iconSize: 40,
            icon: const Icon(
              Icons.add,
            ),
          ),
        ],
      ),
    );
  }
}
