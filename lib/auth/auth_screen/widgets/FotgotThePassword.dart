import 'package:flutter/material.dart';

class ForgotThePassword extends StatelessWidget {
  const ForgotThePassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      style: ButtonStyle(
        overlayColor: MaterialStateProperty.all(
            Colors.transparent), // Убираем эффект нажатия
      ),
      child: const Text(
        'Забыли пароль?',
        style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6)),
      ),
    );
  }
}
