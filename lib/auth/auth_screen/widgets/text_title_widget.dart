import 'package:flutter/material.dart';

class TextTitleWidget extends StatelessWidget {
  final String text;
  final double size;
  const TextTitleWidget({
    Key? key,
    required this.text,
    this.size = 20.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontSize: size, fontWeight: FontWeight.bold),
    );
  }
}
