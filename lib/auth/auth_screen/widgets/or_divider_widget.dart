import 'package:flutter/material.dart';

class OrDividerWidget extends StatelessWidget {
  final double height;
  final double width;
  final double textSize;
  final Color color;
  final Color colorDivider;
  final String text;

  const OrDividerWidget({
    super.key,
    required this.text,
    this.height = 1,
    this.width = 40,
    this.textSize = 13.0,
    this.color = Colors.black,
    this.colorDivider = Colors.grey,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Divider(
            color: colorDivider,
            height: height,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: Text(
            text,
            style: TextStyle(color: color, fontSize: textSize),
          ),
        ),
        Expanded(
          child: Divider(
            color: colorDivider,
            height: height,
          ),
        ),
      ],
    );
  }
}
