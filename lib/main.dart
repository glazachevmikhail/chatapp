import 'package:chatapp/auth/auth_page/info_page_screen.dart';
import 'package:chatapp/auth/auth_screen/auth_screen.dart';
import 'package:flutter/material.dart';
import 'auth/auth_page_mobile/auth_page_mobile_widget.dart';

void main() {
  const myApp = MyApp();
  runApp(myApp);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth >= 800) {
            // Если ширина экрана >= 800, то отображаем виджет A
            return Row(
              children: const [
                Expanded(child: InfoPageScreenWidget()),
                Expanded(child: AuthScreenWidget()),
              ],
            );
          } else {
            // Иначе отображаем виджет B
            return AuthPageMobileWidget();
          }
        },
      ),
    );
  }
}
